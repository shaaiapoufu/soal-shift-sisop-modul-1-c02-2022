#!/bin/bash
register () {
    date=$(date '+%m/%d/%y %H:%M:%S')
    echo "==============================================="
    echo "Please register your account!"
    echo "==============================================="
    echo "Enter username: "
    while true; do
        read username
        if [ ${#username} -eq 0 ]; then
            echo "Enter username: "
            continue
        fi
        find="$(grep "^$username " users/user.txt)"
        if [ ${#find} -eq 0 ]; then
            echo "Enter password: "
            read -r -s password
            pass="${#password}"
            if [ $pass -ge 8 ]; then
                echo "$password" | grep -q [0-9]
                if test $? -eq 0; then
                    echo "$password" | grep -q [A-Z]
                    if test $? -eq 0; then
                        echo "$password" | grep -q [a-z]
                        if test $? -eq 0; then
                            if [ $password != $username ]; then
                                break
                            else 
                                echo "Password should not be the same as username"
                                echo "Enter a new password: "
                                read -r -s password
                            fi
                        else
                            echo "Password should have lower case character"
                            echo "Enter a new password: "
                            read -r -s password
                        fi
                    else
                        echo "Password should have upper case character"
                        echo "Enter a new password"
                        read -r -s password
                    fi
                else
                    echo "Password should contain at least one number"
                    echo "Enter a new password"
                    read -r -s password
                fi
            else 
                echo "Password length should be greater than 8 character"
                echo "Enter a new password: "
                read -r -s password
            fi
        else
            echo "Your username is already exist"
            echo $date "REGISTER: ERROR User already exists" >> log.txt
            echo "Enter a new username: "
        fi    
    done

    echo "==============================================="
    echo "Your account has been successfully registered"
    echo $date "REGISTER: INFO User" $username "registered succesfully" >> log.txt
    echo $username $password >> users/user.txt
}
if [ ! -d users/ ]; then
    mkdir users/ && touch users/user.txt
    register
else
    register
fi