#!/bin/bash
date=$(date '+%m/%d/%y %H:%M:%S')
echo "==============================================="
echo "Please login to your account!"
att=0
while true; do
    echo "==============================================="
    echo "Enter username:"
    read username
    len="${#username}"
    # echo $uname
    find="$(grep "^$username " users/user.txt)"
    find_len="${#find}"
    # echo "$find $find_len"
    if [ $find_len -eq 0 ]; then
        echo "Your username is incorrect"
    else    
        echo "Enter your password:"
        read -r -s password
        arr=($find)
        if [ ${arr[1]} != $password ]; then
            echo "Your password is incorrect"
            echo $date "LOGIN: ERROR Failed login attempt on user" $username >> log.txt
            
        else 
            echo "==============================================="
            echo "You are successfully logged in"
            echo $date "LOGIN: INFO User" $username "logged in" >> log.txt
            break
        fi
    fi
done
while true; do
    filepath="/home/elshervnn/Documents/shift/soal-shift-sisop-modul-1-c02-2022/soal1"
    date_fol=$(date '+%Y-%m-%d')
    echo "==============================================="
    echo "Enter a command:"
    read command
    str=($command)
    cmd=${str[0]}
    num=${str[1]}
    if [ $cmd == "dl" ]; then
        foldername="$date_fol""_""$username"
        filezipname="$foldername"".zip"
        filezipdir="$foldername""/"
        if [ -f $filezipname ]; then
            unzip -P "$password" "$filezipname"
        else
            mkdir "$foldername"
        fi
        count="$(ls $foldername | wc -l)"
        filepath="$filepath""$foldername""/"
        for ((i=$count+1; i<=$num+$count; i++))
        do
            if [ $num -lt 10 ]; then
                filename="$filepath""PIC_0$i"
            else 
                filename="$filepath""PIC_$i"
            fi
            wget -q -O "$filename" https://loremflickr.com/320/240
            echo "==============================================="
            echo "$filename: Downloaded"
        done
        zip --password "$password" -r "$filezipname" "$filezipdir"
        rm -rf "$filezipdir"
        break
    elif [ $cmd == "att" ]; then
        var_e="$(grep -o -i "LOGIN: ERROR Failed login attempt on user $username" log.txt | wc -l)"
        var_s="$(grep -o -i "LOGIN: INFO User $username logged in" log.txt | wc -l)"
        var=$(($var_e+$var_s))
        echo "==============================================="
        echo "Login attemps:" $var
        break
    else
        echo "Please enter a correct command" \[dl\|att\]
    fi
done