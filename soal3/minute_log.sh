#!/bin/bash
date=$(date '+%Y%m%d%H%M%S')
filepath="/home/$USER/log/"
targetpath="/home/$USER"
logfile="$filepath""metrics_""$date"".log"
sudo mkdir -p $filepath
chmod 700 $filepath
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" | sudo tee -a "$logfile" 
{ free -m | awk '{if(NR==2){printf $2","$3""$4","$5","$6","$7","}if(NR==3){printf $2","$3","$4","}}' ; du -shm $targetpath | awk '{printf $2","$1"M\n"}' ; } | sudo tee -a "$logfile" 
chmod 400 $logfile