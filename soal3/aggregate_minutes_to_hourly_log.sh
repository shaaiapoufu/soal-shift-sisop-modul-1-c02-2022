#!/bin/bash
date=$(date '+%Y%m%d%H%M%S')
filepath="/home/$USER/log/"
targetpath="$(find /home/"$USER"/log/*metrics_"$(date '+%Y%m%d%H')"*)"
logfile="$filepath""metrics_agg_""$date"".log"
sudo mkdir -p $filepath
chmod 700 $filepath
printf "minimum," | sudo tee -a "$logfile"  && { readarray -t fs <<< $targetpath; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | head -1 | sudo tee -a "$logfile" 
printf "maksimum," | sudo tee -a "$logfile"  && { readarray -t fs <<< $targetpath; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | tail -1 | sudo tee -a "$logfile" 
{ readarray -t fs <<< $targetpath ;for i in "${fs[@]}" ; do sed -n 2p "$i"; done; } | awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' | sudo tee -a "$logfile" 
chmod 400 $logfile
