# Soal-Shift-Sisop-Modul-1-C02-2022
## Anggota Kelompok
| NRP        | Nama                         |
|------------|------------------------------|
| 5025201050 | Elshe Erviana Angely         |
| 5025201044 | Khariza Azmi Alfajira Hisyam |
| 5025201137 | Maisan Auliya                |
## Daftar Isi
- [Revisi](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#revisi) <br />
- [Soal 1](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#soal-1) <br />
- [Soal 2](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#soal-2) <br />
- [Soal 3](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#soal-3) <br />
## Revisi
Pada saat demo kelompok kami mendapat revisi untuk problem 3.b dan 3.d. <br />
**Problem 3.b** <br />
Kendala yang dialami adalah cron tab tidak berjalan meskipun setelah dicek dengan `sudo service cron start` menampilkan running. Langkah yang digunakan untuk mengatasi adalah:
1. Memindahkan file script ke dalam `/home/$USER` karena crontab tidak bisa membaca filepath.
2. Memberikan `<newline>` pada akhir file crontab, sehingga isi file crontab adalah sebagai berikut:
    ![crontab](/uploads/ce0a2b8f0c3c338565bb4f3fb9f138d2/image.png) <br />
    Hasilnya dapat diketahui dari `/home/$USER/log/` yaitu sebagai berikut: <br />
    ![hasil-cron](/uploads/52f7e38b7b68bbfcbb3b2c9a20bd08ff/image.png) <br />

**Problem 3.d** <br />
Untuk problem 3.d hanya perlu mengganti file permission menjadi read-only untuk owner/user, sehingga perlu menambahkan: `chmod 700 $filepath && chmod 400 $logfile` untuk kedua file. `$filepath` merupakan direktori dari tempat menyimpan log yaitu `/home/$USER/log/`, sedangkan `$logfile` merupakan file log. Hasilnya didapatkan sebagai berikut: <br />
    ![image](/uploads/27587ae19166f3de47f0321bf96b65df/image.png) <br />
## Laporan Resmi
### Soal 1
#### Penjelasan
1. Pada soal 1 a, kami diminta untuk membuat sistem register dan login menggunakan script bash dengan nama file `register.sh` dan `main.sh`, user yang telah membuat akun akan disimpan dalam `users/user.txt`. Data `$username` yang disimpan akan dicari menggunakan `grep` untuk menyocokkan dengan file `users/user.txt`.
2. Membuat password dengan kriteria tertentu menggunakan kondisi `-ge8`, `grep -q [0-9]`, `grep -q [A-Z]`, `grep -q [a-z]`, dan `$username != $password` 
3. Mencatat percobaan register dan login kedalam file `log.txt`. Dicatat sesuai format dengan mengambil nilai dari fungsi `date '+%m/%d/%y %H:%M:%S'` pada linux.
    - Menggunakan `(grep "^$username " users/user.txt` untuk mengambil nama user yang sudah melakukan registrasi, apabila sudah ada maka akan mencetak `"REGISTER: ERROR User already exists"` ke dalam `log.txt`
    - Register akan berhasil apabila password sesuai dengan kriteria pada poin nomor 2 kemudian akan mencetak
    `"REGISTER: INFO User" $username "registered succesfully`
    - Mengambil nilai password dengan `${arr[1]} != $password` apabila password tidak sesuai akan mencetak `"LOGIN: ERROR Failed login attempt on user" $username` apabila berhasil login akan mencetak `"LOGIN: INFO User" $username "logged in"  `
4. Mengetikkan 2 command
    - `dl N` untuk mendownload file ke dalam direktori sesuai format kemudian melakukan zip, apabila sudah ada direktori yang sama maka akan melakukan unzip.
    - `att` untuk menghitung login attemp baik yang berhasil maupun gagal menggunakan `grep -o - i | wc -l`
#### Penyelesaian
**Demo**
1. Pengecekan password sesuai kriteria <br />
    Kode <br />
    ```sh
    if [ $pass -ge 8 ]; then
                echo "$password" | grep -q [0-9]
                if test $? -eq 0; then
                    echo "$password" | grep -q [A-Z]
                    if test $? -eq 0; then
                        echo "$password" | grep -q [a-z]
                        if test $? -eq 0; then
                            if [ $password != $username ]; then
                                break
                            ...
    ```
    ![cek-password](/uploads/a50f22f467c1510baac7838e31e97ef3/1.02.png) <br />
2. Sistem register akan mengambil nilai `$username` dari `users/user.txt` menggunakan grep untuk mengetahui username yang exist. <br />
    Kode <br />
    ```sh
    while true; do
        read username
        if [ ${#username} -eq 0 ]; then
            echo "Enter username: "
            continue
        fi
        find="$(grep "^$username " users/user.txt)"
        ...
        else
            echo "Your username is already exist"
            echo $date "REGISTER: ERROR User already exists" >> log.txt
            echo "Enter a new username: "
        fi    
    done
    ```
    ![cek username exist](/uploads/9a03752ae90bcee3072086702e3543d0/1.01.png) <br/>
3. Menulis log ke dalam `log.txt` untuk register dan login <br />
    Kode <br />
    ```sh
    echo $date "REGISTER: INFO User" $username "registered succesfully" >> log.txt
    echo $username $password >> users/user.txt
    ...
    if [ $find_len -eq 0 ]; then
        echo "Your username is incorrect"
    else    
        echo "Enter your password:"
        read -r -s password
        arr=($find)
        if [ ${arr[1]} != $password ]; then
            echo "Your password is incorrect"
            echo $date "LOGIN: ERROR Failed login attempt on user" $username >> log.txt
            
        else 
            echo "==============================================="
            echo "You are successfully logged in"
            echo $date "LOGIN: INFO User" $username "logged in" >> log.txt
            break
        fi
    fi
    ```
    ![user-failed](/uploads/5933f765a5fbc432e577aea1fb13e937/1.03.png) ![log-file](/uploads/c0b77bed0cb688cdf0976d03097fc9bd/1.04.png) <br />
4. Melakukan perintah `dl N` pada main <br />
    Kode <br />
    ```sh
    if [ $cmd == "dl" ]; then
        foldername="$date_fol""_""$username"
        filezipname="$foldername"".zip"
        filezipdir="$foldername""/"
        if [ -f $filezipname ]; then
            unzip -P "$password" "$filezipname"
        else
            mkdir "$foldername"
        fi
        count="$(ls $foldername | wc -l)"
        filepath="$filepath""$foldername""/"
        for ((i=$count+1; i<=$num+$count; i++))
        do
            if [ $num -lt 10 ]; then
                filename="$filepath""PIC_0$i"
            else 
                filename="$filepath""PIC_$i"
            fi
            wget -q -O "$filename" https://loremflickr.com/320/240
            echo "==============================================="
            echo "$filename: Downloaded"
        done
        zip --password "$password" -r "$filezipname" "$filezipdir"
        rm -rf "$filezipdir"
        break
    ```
    ![dl-command](/uploads/9eb7fefcd2fbbad9ff8ca5b9f7d8eda2/1.05.png) <br />
5. Melakukan perintah `att` pada main <br />
    Kode <br />
    ```sh
    elif [ $cmd == "att" ]; then
        var_e="$(grep -o -i "LOGIN: ERROR Failed login attempt on user $username" log.txt | wc -l)"
        var_s="$(grep -o -i "LOGIN: INFO User $username logged in" log.txt | wc -l)"
        var=$(($var_e+$var_s))
        echo "==============================================="
        echo "Login attemps:" $var
        break
    ```
    ![att](/uploads/f3a2a70be407e32ae2acad994ae96474/1.06.png)
    
### Soal 2
#### Penjelasan
Pada soal 2 ini terdapat 5 problem yang harus diselesaikan yaitu:
1. Problem a, membuat folder bernama `forensic_log_website_daffainfo_log` dengan `mkdir $folderlog`
2. Problem b, mencari rata-rata request per jam dengan menggunakan `cat $log | awk -F: 'NR!=1 {gsub(/"/, "", $3) arr[$3]++}` kemudian membagi hasilnya dengan `hours-1` karena didapatkan 13 angka unique untuk jam. `NR!=1` berarti dimulai dari baris selain 1. Kemudian memasukkan hasil kedalam `ratarata.txt` dengan menggunakan direct.
3. Problem c, mencari ip terbanyak yang melakukan request. Perulangan dilakukan dengan mencari `ip` yang unique sebanyak `max`, karena didapatkan max request adalah 539 maka perulangan selesai. Hasil disimpan dalam `result.txt`.
4. Problem d, mencari user agent yang melakukan request dengan curl menggunakan `cat $log | awk '/curl/ {count++}` nilai count akan bertambah apabila menemukan `/curl/`.
5. Problem 5, menentukan ip yang melakukan penyerangan pada jam 2 pagi, ip yang dicari adalah ip unique, sehingga tidak perlu menulis semua. Di problem ini digunakan `cat $log | awk -F: '/2022:02/ {gsub(/"/, "", $1) arr[$1]++}`.
#### Penyelesaian
**Demo Hasil** <br />
![soal2](/uploads/63b1d98990e36fd09f5f3b00d8ec6d48/image.png) <br />

### Soal 3
#### Penjelasan
1. Problem a, membuat file log bernama `metrics_YmdHMS.log` kemudian mengisi file dengan hasil `awk` terhadap command `du -sh` dan `free-m`.
2. Problem b, **Revisi** ada pada [Revisi](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#revisi)
3. Problem c, mencatat agregat dengan isi nilai minimum, maksimum, dan rata-rata pada `metrics_agg_{YmdH}.log` menggunakan `find tail && find sort` serta menyimpan dalam bentuk array.
4. Problem d, **Revisi** ada pada [Revisi](https://gitlab.com/shaaiapouf/soal-shift-sisop-modul-1-c02-2022#revisi)
#### Penyelesaian
**Demo Hasil** <br />
![soal 3](/uploads/c61080cad2060541239768ffd107402c/image.png) <br />
## Kendala
![confuse](https://c.tenor.com/U3QqFdI8svYAAAAC/pulp-fiction-confused.gif)

~~saya membuat repo sendiri, mengerjakan soal sendiri, commit sendiri, bikin readme juga sendiri~~

**Soal 1**
1. Ada kesulitan dalam menyocokkan value dari `$username` yang sudah melakukan registrasi
2. Ada kesalahan directory untuk file output apabila directory sudah dibuat
3. Ada kesalahan logic dalam mengerjakan problem 1d sehingga penambahan file yang sudah didownload setelah wget salah

**Soal 2**
1. Ada kesulitan untuk menghitung rata-rata attack dalam satu jam karena `$hours` menghitung countunique dari jam. Sehingga `$hours` memiliki nilai 13
2. Looping untuk mencari nilai banyaknya ip tertinggi yang melakukan attack tidak teratur.

**Soal 3**
1. Hasil scrap yang didapatkan untuk nilai maksimum dan minimum dari disk yang digunakan tidak menghitung nilai koma yang terakhir.

## Solusi
**Soal 1**
1. Menggunakan `grep "^$username "` sehingga akan mengambil nilai `$username` dan berhenti setelah `<spasi>`
2. Menggunakan pengkondisian apabila directory belum dibuat maka akan menggunakan `mkdir users/ && touch users/user.txt`baru memanggil fungsi register
3. Menggunakan `count="$(ls $foldername | wc -l)"` untuk mengambil banyaknya file yang sudah didownload kemudian melakukan download ulang dengan index `$count+1`

**Soal 2**

1. Mengurangi nilai yang didapatkan oleh `$hours` dengan 1
2. Menyocokkan nilai ip yang didapat dari file `log_website_daffainfo.log`

**Soal 3**
1. Menggunakan satuan `M` sehingga tidak terdapat tanda koma sebagai separator
