#!/bin/bash
folderlog="forensic_log_website_daffainfo_log/"
log="log_website_daffainfo.log"
if [ ! -d $folderlog ]; then
    mkdir $folderlog
else
    rm -rf $folderlog
    mkdir $folderlog
fi

get_att_per_hour () {
    cat $log | awk -F: 'NR!=1 {gsub(/"/, "", $3) arr[$3]++}
	END {
        for (i in arr) {
            hours++
            res+=arr[i]
        }
        hours=hours-1
        res=res/hours
        printf "Rata-rata serangan adalah sebanyak %.3f requests per jam", res
    }' > $folderlog/ratarata.txt   
}

get_max_ip () {
    cat $log | awk -F: 'NR!=1 {gsub(/"/, "", $1) arr[$1]++}
    END {
        max=0
        for (i in arr){
            # check arr[i] request in i ip
            # print arr[i] " " i
			if (max < arr[i]){
				ip = i
				max = arr[ip]
			}
		}
        printf "IP yang paling banyak mengakses server adalah: " ip " sebanyak " max " requests"
    }' >> $folderlog/result.txt
}

get_curl () {
    cat $log | awk '/curl/ {count++} END {
        printf "\n\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", count
    }' >> $folderlog/result.txt
}


get_ip_two_am () {
    cat $log | awk -F: '/2022:02/ {gsub(/"/, "", $1) arr[$1]++}
    END {
        for (i in arr){
            print i
        }
    }' >> $folderlog/result.txt
}

get_att_per_hour
get_max_ip
get_curl
get_ip_two_am